import 'package:flutter/material.dart';
import 'package:movies/commons/lang.dart';
import 'package:movies/commons/unit.dart';
import 'package:movies/ui/views/detail.dart';
import 'package:flutter/services.dart';

import 'package:movies/ui/views/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rocka Movies',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (RouteSettings settings){
        WidgetBuilder builder;
        dynamic args = settings.arguments;

        switch(settings.name){
          case '/':
            builder = (BuildContext context) => MyHomePage(title: t('list->allMovies'));
            setupSystemColor(Colors.transparent);
            break;
          case '/detail-movie':
            Size windowSize = MediaQuery.of(args['context']).size;
            Offset position = args['position'];
            Alignment alignment = new Alignment(
              (position.dx * 2 / windowSize.width) - 1,
              (position.dy * 2 / windowSize.height) - 1,
            );
            setupSystemColor(Colors.black);
            return ScaleRoute(
              page: DetailMovie(
                position: position,
                movie: args['movie'],
                windowSize: windowSize
              ),
              alignment: alignment,
              begin: u(100) / windowSize.width
            );
            break;
        }

        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}

void setupSystemColor(Color color){
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: color,
  ));
}

class ScaleRoute extends PageRouteBuilder {
  Widget page;
  Alignment alignment;
  double begin;
  ScaleRoute({this.page, this.alignment, this.begin})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              ScaleTransition(
                alignment: alignment,
                scale: Tween<double>(
                  begin: begin,
                  end: 1.0,
                ).animate(
                  CurvedAnimation(
                    parent: animation,
                    curve: Curves.fastOutSlowIn,
                  ),
                ),
                child: child,
              ),
        );
}