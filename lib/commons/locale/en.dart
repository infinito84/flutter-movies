var english = {
  'list': {
    'allMovies': 'All movies',
    'rating': 'Rating',
    'actors': 'Actors',
    'genre': 'Genre',
    'popularity': 'Popularity'
  },
  'detail': {
    'cast': 'Cast',
    'relatedMovies': 'Related Movies',
    'back': 'Back',
    'next': 'Next'
  }
};