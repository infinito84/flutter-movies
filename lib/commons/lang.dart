import 'package:movies/commons/locale/en.dart';

var locale = english;

String t(String keyLang){
  List<String> keys = keyLang.split('->');
  dynamic currentMap = locale;
  for (String key in keys) {
    if(!currentMap.containsKey(key)) return keyLang;
    if(currentMap[key] is String){
      return currentMap[key] as String;
    }
    currentMap = currentMap[key];
  }
}