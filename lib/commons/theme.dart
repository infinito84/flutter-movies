import 'package:flutter/cupertino.dart';

var theme = {
  'textPrimary': Color(0xff333333),
  'textSecond': Color(0xff828282),
  'textThird': Color(0xffffffff),
  'link': Color(0xfffa9917),
};

class SelectedTheme {
  static Color textPrimary = theme['textPrimary'];
  static Color textSecond = theme['textSecond'];
  static Color textThird = theme['textThird']; 
  static Color link = theme['link'];

  static String fontPrimary = 'SharpGrotesk';
}