import 'package:movies/logic/model/actor.dart';
import 'package:movies/logic/model/genre.dart';

class Movie{
  String title;
  String description;
  double rating = 0;
  List<Genre> genres = new List<Genre>();
  List<Actor> actors = new List<Actor>();
  String imageUrl;
  List<double> ratings = new List<double>();

  static List<Movie> movies = new List<Movie>();

  String getGenres(){
    if(genres.length == 0) return '';
    String text = genres[0].name;
    genres.skip(1).forEach((genre) => text = '$text, ${genre.name}');
    return text;
  }

  String getActors(){
    if(actors.length == 0) return '';
    String text = actors[0].name;
    actors.skip(1).forEach((actor) => text = '$text, ${actor.name}');
    return text;
  }
}