class Genre{
  String name;
  static List<Genre> genres = new List<Genre>();

  Genre(String name){
    this.name = name;
  }

  static Genre getInstance(String name){
    for(Genre genre in Genre.genres){
      if(genre.name == name) return genre;
    }
    return new Genre(name);
  }

  String toString(){
    return name;
  }
}