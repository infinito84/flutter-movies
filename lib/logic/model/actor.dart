class Actor{
  String name;
  static List<Actor> actors = new List<Actor>();

  Actor(String name){
    this.name = name;
  }

  static Actor getInstance(String name){
    for(Actor actor in Actor.actors){
      if(actor.name == name) return actor;
    }
    return new Actor(name);
  }
}