import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:movies/commons/constants.dart';
import 'package:movies/logic/model/actor.dart';
import 'package:movies/logic/model/genre.dart';
import 'package:movies/logic/model/movie.dart';

class FetchMovies{

  static Future<List<Movie>> getMovies() async{
    final response = await http.get(Constants.moviesUrlList);
  
    for (dynamic json in json.decode(response.body)) {
      Movie movie = new Movie();
      movie.title = json['title'];
      movie.description = json['storyline'];
      movie.imageUrl = json['posterurl'];

      for(String genre in json['genres']){
        movie.genres.add(Genre.getInstance(genre));
      }
      for(String actor in json['actors']){
        movie.actors.add(Actor.getInstance(actor));
      }
      for(int rating in json['ratings']){
        movie.rating += rating.toDouble();
        movie.ratings.add(rating.toDouble());
      }
      movie.rating /= movie.ratings.length;
      movie.rating = (movie.rating * 10).ceil() / 10;
      Movie.movies.add(movie);
    }

    return Movie.movies;
  }
}