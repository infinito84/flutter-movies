import 'package:movies/logic/model/movie.dart';

class SimilarMovies{

  static Future<List<Movie>> getMovies(Movie movie) async{
    List errors = new List();
    for(Movie compareMovie in Movie.movies){
      if(movie == compareMovie) continue;
      int error = 0;
      for(int i = 0; i<movie.ratings.length;i++){
        error += (movie.ratings[i] - compareMovie.ratings[i]).abs().toInt();
      }
      errors.add({
        'error': error,
        'movie': compareMovie
      });
    }
    errors.sort((a, b) => a['error'] - b['error']);
    errors = errors.sublist(0, 10);
    return List<Movie>.from(errors.map((item) => item['movie']).toList());
  }
}