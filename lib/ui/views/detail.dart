import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:movies/commons/lang.dart';
import 'package:movies/commons/theme.dart';
import 'package:movies/commons/unit.dart';
import 'package:movies/logic/model/movie.dart';
import 'package:movies/logic/services/similar-movies.dart';
import 'package:movies/ui/components/movie-card.dart';

class DetailMovie extends StatefulWidget {
  DetailMovie({Key key, this.movie, this.position, this.windowSize}) : super(key: key);

  Movie movie;
  Offset position;
  Size windowSize;

  @override
  DetailMovieState createState() => DetailMovieState();
}

class DetailMovieState extends State<DetailMovie> with SingleTickerProviderStateMixin{
  AnimationController controller;
  double width, height;
  Future<List<Movie>> moviePromise;

  @override
  void initState() {
    super.initState();
    width = widget.windowSize.width;
    height = (u(555.04) * widget.windowSize.width / u(375));
    moviePromise = SimilarMovies.getMovies(widget.movie);

    controller = AnimationController(
      duration: new Duration(milliseconds: 500),
      vsync: this
    );
    controller.addListener((){
      this.setState((){
      });
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        decoration: BoxDecoration(
          color: Colors.black
        ),
        child: ListView(
          children: <Widget>[
            imageWidget(),
            descriptionWidget(),
            similarMoviesWidget(),
            navigationWidget(),
          ],
        ),
      )
    );
  }

  Widget imageWidget(){
    Movie movie = widget.movie;

    return Container(
      width: width,
      height: height,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(movie.imageUrl),
            fit: BoxFit.cover
          )
        ),
        child: Container(
          width: width,
          height: width,
          decoration: styles['image']['gradient']
        )
      )
    );
  }

  Widget descriptionWidget(){
    Movie movie = widget.movie;

    return Container(
      padding: EdgeInsets.only(left: u(16), right: u(16)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
        Container(
          padding: EdgeInsets.only(bottom: u(16)),
          child: Text(movie.title, style: styles['description']['title'])
        ),
        Container(
          padding: EdgeInsets.only(bottom: u(16)),
          child: Text('${t('list->rating')}: ${movie.rating}', style: styles['description']['fields'])
        ),
        Container(
          padding: EdgeInsets.only(bottom: u(16)),
          child: Text(movie.getGenres(), style: styles['description']['fields'])
        ),
        Container(
          padding: EdgeInsets.only(bottom: u(16)),
          child: Text(movie.description, style: styles['description']['description'])
        ),
        Container(
          padding: EdgeInsets.only(bottom: u(23)),
          child: Text('${t('detail->cast')}: ${movie.getActors()}', style: styles['description']['cast'])
        )
      ]),
    );
  }

  Widget similarMoviesWidget(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: u(30), bottom: u(24), left: u(16)),
          child: Text(t('detail->relatedMovies'), style: styles['relatedMovies']['title'])
        ),
        Container(
          width: width,
          height: u(220.43) * width / u(375),
          child: FutureBuilder<List<Movie>>(
            future: moviePromise,
            builder: (context, snapshot){
              if(snapshot.hasData){
                List<Movie> movies = snapshot.data;
                return ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.only(left: u(16), right: u(16), bottom: u(18)),
                  itemCount: movies.length,
                  itemBuilder: (context, index) => MovieCard(
                    movie: movies[index],
                    type: MovieCardType.ALTERNATIVE
                  ),
                );
              }
              if(snapshot.hasError){
                return Text("${snapshot.error}", style: styles['description']['title']);
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ],
    );
  }

  Widget navigationWidget(){
    return Container(
      padding: EdgeInsets.only(left: u(16), right: u(16), bottom: u(49)),
      child: Row(
        children: <Widget>[
        Expanded(
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(u(8))),
            child: Container(
              padding: EdgeInsets.only(top: u(16), bottom: u(14)),
              decoration: styles['navigation']['decoration'],
              child: Center(
                child: GestureDetector(
                  child: Text(t('detail->back'), style: styles['navigation']['text'])
                ),
              ),
            ),
          ),
        ),
        SizedBox(width: u(27)),
        Expanded(
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(u(8))),
            child: Container(
              decoration: styles['navigation']['decoration'],
              padding: EdgeInsets.only(top: u(16), bottom: u(14)),
              child: Center(
                child: GestureDetector(
                  child: Text(t('detail->next'), style: styles['navigation']['text'])
                ),
              ),
            ),
          ),
        )
      ]),
    );
  }

  var styles = {
    'image': {
      'gradient': BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset.center,
          end: FractionalOffset.bottomCenter,
          colors: [
            Colors.transparent,
            Colors.black
          ],
          stops: [0, 1]
        )
      )
    },
    'description': {
      'title': TextStyle(
        fontFamily: SelectedTheme.fontPrimary,
        color: SelectedTheme.textThird,
        fontWeight: FontWeight.w700,
        fontSize: u(24)
      ),
      'fields': TextStyle(
        fontFamily: SelectedTheme.fontPrimary,
        fontSize: u(14),
        color: SelectedTheme.link
      ),
      'description': TextStyle(
        fontFamily: SelectedTheme.fontPrimary,
        height: 22/12,
        fontSize: u(12),
        color: SelectedTheme.textThird
      ),
      'cast': TextStyle(
        fontFamily: SelectedTheme.fontPrimary,
        fontSize: u(12),
        color: SelectedTheme.textSecond
      ),
    },
    'relatedMovies': {
      'title': TextStyle(
        fontFamily: SelectedTheme.fontPrimary,
        fontSize: u(20),
        color: SelectedTheme.textThird,
        fontWeight: FontWeight.w500
      )
    },
    'navigation': {
      'decoration': BoxDecoration(
        color: SelectedTheme.textThird
      ),
      'text': TextStyle(
        fontFamily: SelectedTheme.fontPrimary,
        fontSize: u(18),
        color: SelectedTheme.textPrimary,
        fontWeight: FontWeight.w500
      )
    }
  };
}