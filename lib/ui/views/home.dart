import 'package:flutter/material.dart';
import 'package:movies/commons/theme.dart';
import 'package:movies/commons/unit.dart';
import 'package:movies/logic/model/movie.dart';
import 'package:movies/logic/services/fetch-movies.dart';
import 'package:movies/ui/components/movie-card.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<List<Movie>> moviePromise;
  @override
  void initState() {
    super.initState();
    moviePromise = FetchMovies.getMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context),
      body: body(context),
    );
  }

  Widget appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(0, u(111)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: u(16), top: u(60), right: u(16), bottom: u(16)),
            child: styles['appBar']['icon']
          ),
          Container(
            padding: EdgeInsets.only(left: u(16), right: u(16)),
            child: Row(
              children: <Widget>[
                Text(widget.title, style: styles['appBar']['title']),
              ],
            )
          ),
        ],
      ),
    );
  }

  Widget body(BuildContext context){
    return FutureBuilder<List<Movie>>(
      future: moviePromise,
      builder: (context, snapshot){
        if(snapshot.hasData){
          List<Movie> movies = snapshot.data;
          return ListView.builder(
            padding: EdgeInsets.only(left: u(16), right: u(16), bottom: u(18), top: u(62)),
            itemCount: movies.length,
            itemBuilder: (context, index) => MovieCard(movie: movies[index]),
          );
        }
        if(snapshot.hasError){
          return Text("${snapshot.error}");
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  var styles = {
    'appBar': {
      'title': TextStyle(
        color: SelectedTheme.textPrimary,
        fontSize: u(28),
        fontFamily: SelectedTheme.fontPrimary,
        fontWeight: FontWeight.bold
      ),
      'icon': Image.asset(
        'lib/ui/assets/ic-menu.png',
        width: u(24),
        height: u(19),
      )
    }
  };
}
