import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movies/commons/lang.dart';
import 'package:movies/commons/theme.dart';
import 'package:movies/commons/unit.dart';
import 'package:movies/logic/model/actor.dart';
import 'package:movies/logic/model/genre.dart';
import 'package:movies/logic/model/movie.dart';

enum MovieCardType{
  NORMAL,
  ALTERNATIVE
}

class MovieCard extends StatefulWidget {
  MovieCard({Key key, this.movie, this.type}) : super(key: key);
  Movie movie;
  MovieCardType type = MovieCardType.NORMAL;

  @override
  MovieCardState createState() => MovieCardState();
}

class MovieCardState extends State<MovieCard> {
  GlobalKey cardKey = GlobalKey();

  void onTap(context){
    RenderBox box = cardKey.currentContext.findRenderObject();
    Navigator.pushNamed(context, '/detail-movie', arguments: {
      'position': box.localToGlobal(Offset.zero),
      'movie': widget.movie,
      'context': context
    });
  }

  @override
  Widget build(BuildContext context) {
    Movie movie = widget.movie;

    if(widget.type == MovieCardType.ALTERNATIVE){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          imageWidget(),
          Container(
            width: u(116),
            child: Text(
              movie.title,
              overflow: TextOverflow.ellipsis,
              style: styles['titleAlt']
            )
          )
        ]
      );
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        imageWidget(),
        infoWidget(),
    ]);
  }

  Widget imageWidget(){
    Movie movie = widget.movie;

    return ClipRect(
      key: cardKey,
      child: Container(
        width: u(116),
        height: u(179),
        child: Stack(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Positioned(
                  top: u(20),
                  left:u(10),
                  child: Image.network(
                    movie.imageUrl,
                    width: u(80),
                    height: u(150),
                  ),
                ),
                Positioned(
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: BackdropFilter(
                    filter: ui.ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                    child: Container(
                      color: Colors.black.withOpacity(0),
                    ),
                  ),
                )
              ],
            ),
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(u(8))),
              child: GestureDetector(
                onTap: () => onTap(context),
                child: Container(
                  width: u(100),
                  height: u(148),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(movie.imageUrl),
                      fit: BoxFit.cover
                    )
                  )
                ),
              )
            )
          ],
        )
      ),
    );
  }

  Widget infoWidget(){
    Movie movie = widget.movie;

    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(bottom: u(11)),
            child: Text(movie.title, style: styles['title'])
          ),
          movie.rating != null ? Container(
            padding: EdgeInsets.only(bottom: u(7)),
            child: Text('${t('list->rating')}: ${movie.rating}', style: styles['fields'])
          ) : Container(),
          Container(
            padding: EdgeInsets.only(bottom: u(6)),
            child: Wrap(children: getListActors(movie.actors)),
          ),
          Wrap(children: getListGenres(movie.genres)),
        ]),
    );
  }

  List<Widget> getListActors(List<Actor> actors){
    List<Widget> widgets = new List<Widget>();
    widgets.add(Text('${t('list->actors')}: ', style: styles['fields']));
    actors.forEach((Actor actor) => widgets.add(
      Text('${actor.name}, ', style: styles['link'])
    ));
    return widgets;
  }

  List<Widget> getListGenres(List<Genre> genres){
    List<Widget> widgets = new List<Widget>();
    widgets.add(Text('${t('list->genre')}: ', style: styles['fields']));
    genres.forEach((Genre genre) => widgets.add(
      Text('${genre.name}, ', style: styles['link'])
    ));
    return widgets;
  }

  var styles = {
    'title': TextStyle(
      fontFamily: SelectedTheme.fontPrimary,
      fontWeight: FontWeight.w500,
      fontSize: u(17),
      color: SelectedTheme.textPrimary
    ),
    'fields': TextStyle(
      fontFamily: SelectedTheme.fontPrimary,
      fontSize: u(12),
      color: SelectedTheme.textSecond
    ),
    'link': TextStyle(
      fontFamily: SelectedTheme.fontPrimary,
      fontSize: u(12),
      color: SelectedTheme.link
    ),
    'titleAlt': TextStyle(
      fontFamily: SelectedTheme.fontPrimary,
      fontSize: u(12),
      color: SelectedTheme.textThird,
    )
  };
}
